package org.chevron.core;

import java.io.Serializable;

public class TemplatingContextPoolingConfig implements Serializable {

    private volatile int maxIdlePerEngine = Runtime.getRuntime().availableProcessors();
    private volatile int minIdlePerEngine = 0;
    private volatile int maxTotalPerEngine = -1;

    public int getMaxIdlePerEngine() {
        return maxIdlePerEngine;
    }

    public void setMaxIdlePerEngine(int maxIdlePerEngine) {
        this.maxIdlePerEngine = maxIdlePerEngine;
    }

    public int getMinIdlePerEngine() {
        return minIdlePerEngine;
    }

    public void setMinIdlePerEngine(int minIdlePerEngine) {
        this.minIdlePerEngine = minIdlePerEngine;
    }

    public int getMaxTotalPerEngine() {
        return maxTotalPerEngine;
    }

    public void setMaxTotalPerEngine(int maxTotalPerEngine) {
        this.maxTotalPerEngine = maxTotalPerEngine;
    }
}
