package org.chevron.core.composition;

import org.chevron.model.schema.ParamsValidationException;

public class InvalidParamsException extends ParamsValidationException {

    public InvalidParamsException(String message) {
        super(message);
    }

    public InvalidParamsException(String message, Throwable cause) {
        super(message, cause);
    }
}
