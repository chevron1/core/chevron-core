package org.chevron.core.composition;

import org.chevron.model.ChevronException;

public class CompositionException extends ChevronException {

    public CompositionException(String message) {
        super(message);
    }

    public CompositionException(String message, Throwable cause) {
        super(message, cause);
    }
}
