package org.chevron.core.composition;

import java.util.HashMap;

public class CompositionParams extends HashMap<String,Object> {

    private CompositionParams(){
        super();
    }


    public static Builder builder(){

        return new Builder();

    }

    public static class  Builder {

        private CompositionParams params = new CompositionParams();

        private Builder(){
            super();
        }

        public Builder put(String key, Object object){
            this.params.put(key,object);
            return this;
        }

        public CompositionParams build(){
            return this.params;
        }

    }

}
