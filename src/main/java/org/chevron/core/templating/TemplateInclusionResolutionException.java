package org.chevron.core.templating;

import org.chevron.model.ChevronException;

public class TemplateInclusionResolutionException extends ChevronException {

    public TemplateInclusionResolutionException(String message) {
        super(message,null);
    }

    public TemplateInclusionResolutionException(String message, Throwable cause) {
        super(message,cause);
    }

}
