package org.chevron.core.templating;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.chevron.model.Format;
import org.chevron.model.Template;
import org.chevron.model.TemplatesPool;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Represents the inclusion of a {@link Template} into another.
 */
public class TemplateInclusion implements Serializable {

    private String templateName;
    private boolean cascadeQualifiers;

    private static final Logger LOGGER = LogManager.getLogger(TemplateInclusion.class);

    public TemplateInclusion(String templateName){
        this(templateName,false);
    }

    public TemplateInclusion(String templateName, boolean cascadeQualifiers){
        this.templateName = templateName;
        this.cascadeQualifiers = cascadeQualifiers;
    }

    public String getTemplateName() {
        return templateName;
    }

    public boolean isCascadeQualifiers() {
        return cascadeQualifiers;
    }

    public Template resolve(TemplatesPool pool, Format format, List<String> qualifiers) throws TemplateInclusionResolutionException {
        LOGGER.debug("Resolving Include. Template=[{}], Cascade qualifiers=[{}]",getTemplateName(),cascadeQualifiers);
        Optional<Template> templateOptional = Optional.empty();
        try {
            List<String> qualifiersList = new ArrayList<>();
            if(cascadeQualifiers)
                qualifiersList.addAll(qualifiers);
            templateOptional = pool.getTemplate(templateName, format,
                    qualifiersList);
        }catch (Exception ex){
            throw new TemplateInclusionResolutionException("error found while obtaining template from Templates Pool",
                    ex);
        }
        if(!templateOptional.isPresent()){
            if(cascadeQualifiers)
                throw new TemplateNotFoundException(templateName,format,
                        qualifiers);
            else throw new TemplateNotFoundException(templateName,format, Collections.emptyList());
        }

        return templateOptional.get();
    }

    public String toExpression(){
        StringBuilder builder = new StringBuilder();
        builder.append('[');
        if(cascadeQualifiers)
            builder.append("<<");
        else builder.append("<%");
        builder.append(templateName);
        if(cascadeQualifiers)
            builder.append(">>");
        else builder.append("%>");
        builder.append(']');
        return builder.toString();
    }

    public String toString(){
        return toExpression();
    }

}
