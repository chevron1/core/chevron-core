package org.chevron.core.templating;

import java.util.regex.Pattern;

public final class TemplatePatterns {

    public static final Pattern INCLUDE = Pattern.compile("\\[<%([A-Za-z][A-Za-z-]+[A-Za-z])%>\\]",Pattern.DOTALL);
    public static final Pattern INCLUDE_CASCADE_QUALIFIERS = Pattern.compile("\\[<<([A-Za-z][A-Za-z-]+[A-Za-z])>>\\]", Pattern.DOTALL);

}
