package org.chevron.core.templating;

import org.apache.commons.pool2.BaseKeyedPooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.chevron.model.*;

import java.util.Optional;

public class TemplateContextFactory extends BaseKeyedPooledObjectFactory<String,TemplatingContext> {

    private ChevronContext chevronContext;

    public TemplateContextFactory(ChevronContext chevronContext){
        this.chevronContext = chevronContext;
    }

    @Override
    public TemplatingContext create(String language) throws Exception {
        Optional<RegisteredTemplateEngine> optional = this.chevronContext.getTemplateEngine(
                language);
        if(optional.isPresent()){
            RegisteredTemplateEngine registeredTemplateEngine = optional.get();
            TemplateEngine templateEngine = registeredTemplateEngine.getInstance();
            return templateEngine.createContext(
                    registeredTemplateEngine.getConfiguration(), chevronContext
                            .getDefinitions());
        }
        throw new ChevronException("there is no template engine registered for language: "+
                language);
    }

    @Override
    public PooledObject<TemplatingContext> wrap(TemplatingContext templatingContext) {
        return new DefaultPooledObject<>(
                templatingContext);
    }

    public boolean validateObject(String key, PooledObject<TemplatingContext> p) {
        p.getObject().next();
        return true;
    }

}
