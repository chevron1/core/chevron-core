package org.chevron.core.templating;


import org.chevron.model.Format;

import java.util.List;

public class TemplateNotFoundException extends TemplateInclusionResolutionException {

    public TemplateNotFoundException(String name) {
       super(String.format("Template [%s] was not found",name));
    }

    public TemplateNotFoundException(String name, Format format, List<String> qualifiers) {
        super(String.format("%s template [%s] with qualifiers [%s] was not found",format.name(), name,
                toString(qualifiers)));
    }

    private static String toString(List<String> qualifiers){
        StringBuilder builder = new StringBuilder();
        boolean first = true;
        for(String qualifier: qualifiers){
            if(!first)
                builder.append(",");
            builder.append(qualifier);
            first = false;
        }
        return builder.toString();
    }

}
