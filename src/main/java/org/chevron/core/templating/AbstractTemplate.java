package org.chevron.core.templating;

import org.chevron.core.util.MutableString;
import org.chevron.model.ChevronContext;
import org.chevron.model.Template;
import org.chevron.model.TemplatesPool;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

public abstract class AbstractTemplate implements Template {

    //TODO: Ensure that template languages are equal when processing inclusions

    private transient TemplatesPool pool;
    private List<TemplateInclusion> inclusions = new ArrayList<>();
    private boolean inclusionsResolved = false;
    private String processedSource = "";

    public AbstractTemplate(TemplatesPool pool){
        this.pool = pool;
    }

    private List<TemplateInclusion> findInclusions(String content){
        List<TemplateInclusion> templateInclusionList = new ArrayList<>();
        Matcher includeMatcher = TemplatePatterns.INCLUDE.matcher(content);
        while (includeMatcher.find()){
            String templateName = includeMatcher.group(1);
            templateInclusionList.add(new TemplateInclusion(
                    templateName));
        }

        Matcher includeCascadeQualifiersMatcher = TemplatePatterns
                .INCLUDE_CASCADE_QUALIFIERS
                .matcher(content);
        while (includeCascadeQualifiersMatcher.find()){
            String templateName = includeCascadeQualifiersMatcher.group(1);
            templateInclusionList.add(new TemplateInclusion(templateName,true));
        }
        return templateInclusionList;
    }

    private synchronized void resolveInclusions(String source, List<String> qualifiers){
        this.inclusions = findInclusions(source);
        MutableString mutableContent = new MutableString(source);
        for(TemplateInclusion inclusion: inclusions){
            Template resolvedTemplate = null;
            //Resolve template with qualifiers
            if(inclusion.isCascadeQualifiers())
                resolvedTemplate = inclusion.resolve(pool, getFormat(), qualifiers);
            //Resolve template without qualifiers
            else resolvedTemplate = inclusion.resolve(pool, getFormat(), qualifiers);
            //Replace the template include expression with the template content
            mutableContent.replaceBy(inclusion.toExpression(),resolvedTemplate.
                    getSource());
        }
        this.processedSource = mutableContent.toString();
        this.inclusionsResolved = true;
    }

    private void checkInclusions(){
        if(!inclusionsResolved) {
            resolveInclusions(getRawSource(),getQualifiers());
        }
    }

    protected abstract String getRawSource();

    public String getSource(){
        this.checkInclusions();
        return this.processedSource;
    }

}
