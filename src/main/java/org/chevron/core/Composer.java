package org.chevron.core;

import org.apache.commons.pool2.impl.GenericKeyedObjectPool;
import org.apache.commons.pool2.impl.GenericKeyedObjectPoolConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.chevron.core.composition.CompositionParams;
import org.chevron.core.templating.TemplateContextFactory;
import org.chevron.core.templating.TemplateNotFoundException;
import org.chevron.core.util.Utils;
import org.chevron.model.*;
import org.chevron.model.schema.ObjectSchema;
import org.chevron.model.schema.ObjectSchemaRef;

import java.util.List;
import java.util.Optional;


public final class Composer {

    private ChevronContext context;

    private GenericKeyedObjectPool<String, TemplatingContext> templatingContextPool;

    private static final Logger LOGGER = LogManager.getLogger(Composer.class);

    public Composer(ChevronContext context){
        this(context, new TemplatingContextPoolingConfig());
    }

    public Composer(ChevronContext context, TemplatingContextPoolingConfig poolingConfig){
        if(context==null)
            throw new IllegalArgumentException("context must not be null");
        if(poolingConfig==null)
            throw new IllegalArgumentException("poolingConfig must not be null");
        this.context = context;
        this.templatingContextPool = new GenericKeyedObjectPool<>(new TemplateContextFactory(
                context),makeConfig(poolingConfig));
    }

    private GenericKeyedObjectPoolConfig<TemplatingContext> makeConfig(TemplatingContextPoolingConfig config){
        GenericKeyedObjectPoolConfig<TemplatingContext> poolConfig = new GenericKeyedObjectPoolConfig<>();
        poolConfig.setMaxIdlePerKey(config.getMaxIdlePerEngine());
        poolConfig.setMaxTotalPerKey(config.getMaxTotalPerEngine());
        poolConfig.setMinIdlePerKey(config.getMinIdlePerEngine());
        return poolConfig;
    }

    public String compose(String templateName, Format format,  List<String> qualifiers, CompositionParams params){
        if(templateName==null)
            throw new IllegalArgumentException("template name must not be null nor empty");
        if(format==null)
            throw new IllegalArgumentException("format must not be null");
        if(params==null)
            throw new IllegalArgumentException("params must not be null");
        Template template = findTemplate(templateName,format,qualifiers);
        String language = template.getLanguage();
        LOGGER.info("Template Language=[{}]",language);
        this.validateInternal(template,params);
        String source = template.getSource();
        TemplatingContext templatingContext = null;
        try {
            LOGGER.info("Obtaining [{}] templating context",language);
            templatingContext = templatingContextPool.borrowObject(template.getLanguage());
            LOGGER.info("Templating context obtained. Merging template with params");
            String output = templatingContext.merge(source, params);
            LOGGER.info("Template merged successfully with params");
            return output;
        }catch (Exception ex){
            throw new ChevronException("Error obtaining templating context from Pool. Template Language: "+
                    language);
        }
    }


    public void validate(String templateName, Format format,  List<String> qualifiers, CompositionParams params){
        if(templateName==null)
            throw new IllegalArgumentException("template name must not be null nor empty");
        if(format==null)
            throw new IllegalArgumentException("format must not be null");
        if(params==null)
            throw new IllegalArgumentException("params must not be null");
        Template template = findTemplate(templateName,format,qualifiers);
        this.validateInternal(template,params);
    }

    private void validateInternal(Template template, CompositionParams params){
        ObjectSchemaRef objectSchemaRef = template.getSchema();
        LOGGER.info("Resolving Object schema: [{}]",objectSchemaRef.getSchemaName());
        ObjectSchema objectSchema = objectSchemaRef.resolve(context.getObjectSchemasPool());
        LOGGER.info("Object schema resolved");
        LOGGER.info("Validating composition params");
        objectSchema.validate(params);
    }

    private Template findTemplate(String templateName, Format format, List<String> qualifiers){
        LOGGER.info("Fetching Template from Pool: Name=[{}] Format=[{}], Qualifiers={}",templateName,format.name(), Utils.listToString(
                qualifiers));
        Optional<Template> optional = context.getTemplatesPool().getTemplate(
                templateName, format,qualifiers);
        if(!optional.isPresent()) {
            LOGGER.warn("Template not found in the Pool");
            throw new TemplateNotFoundException(templateName, format, qualifiers);
        }
        LOGGER.info("Template found");
        return optional.get();
    }

}
