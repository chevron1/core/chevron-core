package org.chevron.core.util;

import java.util.List;

public class Utils {

    public static String listToString(List objects){
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        boolean first = true;
        for(Object object: objects){
            if(!first) {
                builder.append(",");
                builder.append(object.toString());
            }else {
                builder.append(object.toString());
                first = false;
            }
        }
        builder.append("]");
        return builder.toString();
    }

}
