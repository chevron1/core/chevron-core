package org.chevron.core.util;

public class MutableString {

    private String value = "";

    public MutableString(String value){
        this.value = value;
    }

    public MutableString regexpReplaceBy(String regexp, String content){
        if(regexp==null||regexp.isEmpty())
            throw new IllegalArgumentException("regexp must not be null nor empty");
        this.value = this.value.replaceAll(regexp,content);
        return this;
    }

    public MutableString replaceBy(String what, String by){
        this.value = this.value.replace(what, by);
        return this;
    }

    public String toString(){

        return this.value;

    }

}
