package org.chevron.core.util;

import java.io.Serializable;
import java.util.HashMap;

public class KeyValueObject extends HashMap<String,Object> implements Serializable {

    private KeyValueObject(){
        super();
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder {

        private KeyValueObject keyValueObject = new KeyValueObject();

        private Builder(){}

        public Builder put(String name, Object param){
            this.keyValueObject.put(name, param);
            return this;
        }

        public KeyValueObject build(){
            return keyValueObject;
        }

    }

}
